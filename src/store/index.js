import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cartModal: false,
    popularBeer: []
  },
  mutations: {
    SET_CART_MODAL(state, payload) {
      state.cartModal = payload;
    },
    SET_POPULAR_BEER(state, payload) {
      state.popularBeer = payload;
    }
  },
  actions: {
    showCartModal(context, payload) {
      context.commit('SET_CART_MODAL', payload);
    },
    getPopularBeer(context) {
      axios.get('/api/populars').then((res) => {
        context.commit('SET_POPULAR_BEER', res.data.populars);
      }).catch((err) => {
        console.error(err);
      });
    }
  },
  getters: {
    getUSAPopularBeer: state => {
      return state.popularBeer.filter(beer => beer.country === 'USA');
    },
  }
});

// context -> state, commit, dispatch
// state
// commit
// dispatch


