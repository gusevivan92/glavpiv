import { createServer, Model } from "miragejs"

export function makeServer({ environment = "development" } = {}) {
  let server = createServer({
    environment,

    models: {
      popular: Model,
    },

    seeds(server) {
      server.create("popular", {
        name: "Konix",
        country: "USA",
        volume: "0.5",
        price: 298,
        image: "https://dummyimage.com/190x190/c1c1c1/000000",
      });
      server.create("popular",  {
        name: "Rewort",
        country: "Россия",
        volume: "0.5",
        price: 298,
        image: "https://dummyimage.com/190x190/c1c1c1/000000",
      });
      server.create("popular",  {
        name: "Rewort",
        country: "Россия",
        volume: "0.5",
        price: 298,
        image: "https://dummyimage.com/190x190/c1c1c1/000000",
      });
      server.create("popular",  {
        name: "Rewort",
        country: "Россия",
        volume: "0.5",
        price: 298,
        image: "https://dummyimage.com/190x190/c1c1c1/000000",
      });
      server.create("popular",  {
        name: "Rewort",
        country: "Россия",
        volume: "0.5",
        price: 298,
        image: "https://dummyimage.com/190x190/c1c1c1/000000",
      });
      server.create("popular",  {
        name: "Rewort",
        country: "Россия",
        volume: "0.5",
        price: 298,
        image: "https://dummyimage.com/190x190/c1c1c1/000000",
      });
      server.create("popular",  {
        name: "Rewort",
        country: "Россия",
        volume: "0.5",
        price: 298,
        image: "https://dummyimage.com/190x190/c1c1c1/000000",
      });
   
    },

    routes() {
      this.namespace = "api"

      this.get("/populars", (schema) => {
        return schema.populars.all()
      })
    },
  })

  return server
}